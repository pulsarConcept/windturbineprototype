using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;

public class BuilderScript{
    const string VERSION_NUMBER = "0.1";

    static string[] SCENES = FindEnabledEditorScenes();
	
	static string APP_NAME = "prevencion";
	static string TARGET_DIR = "C:\\Proyectos\\CIE\\Products\\WindTurbine\\";
	
	[MenuItem ("Custom/CI/Build iPhone")]
	
	static void PerformIOSBuild ()
	{   
		string target_dir = APP_NAME;
		GenericBuild(SCENES, TARGET_DIR + "IOS/" + VERSION_NUMBER +"\\"+ target_dir, BuildTarget.iOS,BuildOptions.None);
	}
	
	[MenuItem ("Custom/CI/Build Android")]
	
	static void PerformAndroidBuild ()
	{   
		string target_dir = APP_NAME + ".apk";
		GenericBuild(SCENES, TARGET_DIR + "Android/" + VERSION_NUMBER +"\\"+ target_dir, BuildTarget.Android,BuildOptions.None);
	}
	
	static void PerformMacBuild ()
	{   
		string target_dir = APP_NAME + ".app";
		GenericBuild(SCENES, TARGET_DIR + "Mac/" + VERSION_NUMBER +"\\"+ target_dir, BuildTarget.StandaloneOSXUniversal,BuildOptions.None);
	}
	
	static void PerformWindowsBuild ()
	{   
		string target_dir = APP_NAME + ".exe";
		GenericBuild(SCENES, TARGET_DIR + "Windows\\"+ VERSION_NUMBER +"\\"+ target_dir, BuildTarget.StandaloneWindows64,BuildOptions.None);
	}
	
	private static string[] FindEnabledEditorScenes() {
		List<string> EditorScenes = new List<string>();
		foreach(EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (!scene.enabled) continue;
			EditorScenes.Add(scene.path);
		}
		return EditorScenes.ToArray();
	}
	
	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{   
		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		string res = BuildPipeline.BuildPlayer(scenes,target_dir,build_target,build_options);
		if (res.Length > 0) {
			throw new Exception("BuildPlayer failure: " + res);
		}
	}
	
}

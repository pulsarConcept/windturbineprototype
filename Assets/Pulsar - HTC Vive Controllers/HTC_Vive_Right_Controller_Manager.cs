﻿using UnityEngine;

public class HTC_Vive_Right_Controller_Manager : MonoBehaviour {

    enum States{
        unplugged,
        idle,
        triggerPressed,
        padPressed
    }
    States controllerState = new States();

    void Awake(){
        controllerState = States.unplugged;
    }

    void OnDisable(){
        HTC_Vive_Controllers_Manager.instance.rightController = 0;
        controllerState = States.unplugged;
    }

    void Update(){
        if (HTC_Vive_Controllers_Manager.instance.rightController != 0 && controllerState != States.unplugged){
            ReleasePad();
            PressPad();
            ReleaseTrigger();
            PressTrigger();
        }else if (controllerState == States.unplugged && HTC_Vive_Controllers_Manager.instance.rightController != 0){
            SetControllerIndex();
        }
    }

    void SetControllerIndex(){
        if (checked((int) HTC_Vive_Controllers_Manager.instance.rightController) == 1){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device1;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.rightController) == 2){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device2;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.rightController) == 3){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device3;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.rightController) == 4){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device4;
        }
        controllerState = States.idle;
    }

    void PressPad(){
        if (HTC_Vive_Controllers_Manager.instance.TouchPadIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.rightController) && controllerState != States.padPressed && controllerState != States.triggerPressed){
            GetComponent<HTC_Vive_LasterPointer>().EnableLaser();
            controllerState = States.padPressed;
        }
    }

    void ReleasePad(){
        if (!HTC_Vive_Controllers_Manager.instance.TouchPadIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.rightController) && controllerState == States.padPressed){
            GetComponent<HTC_Vive_LasterPointer>().DisableLaser();
            GameObject.Find("Teleporter").GetComponent<HTC_Vive_Teleport_Manager>().TeleportToRayCastHit();
            controllerState = States.idle;
        }
    }

    void PressTrigger(){
        if (HTC_Vive_Controllers_Manager.instance.TriggerIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.rightController) && controllerState != States.triggerPressed && controllerState != States.padPressed){
            GameObject.Find("Teleporter").GetComponent<HTC_Vive_Teleport_Manager>().TeleportToSpawnPoints();
            controllerState = States.triggerPressed;
        }
    }

    void ReleaseTrigger(){
        if (!HTC_Vive_Controllers_Manager.instance.TriggerIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.rightController) && controllerState == States.triggerPressed){
            controllerState = States.idle;
        }
    }
}


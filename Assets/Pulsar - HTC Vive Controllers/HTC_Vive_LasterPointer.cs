﻿using UnityEngine;

public class HTC_Vive_LasterPointer : MonoBehaviour {

    public bool raycastDidHit { get; private set; }
    public Material[] materials = new Material[2];

    private bool active = false;
    private Ray raycast = new Ray();
    private Color color;
    private float thickness = 0.004f;
    private float distance = 100f;

    public GameObject holder;
    public GameObject pointer;
    public bool addRigidBody = false;
    public Transform reference;
    public GameObject tpArea;
    public GameObject notTpArea;
    public event PointerEventHandler PointerIn;
    public event PointerEventHandler PointerOut;

    Transform previousContact = null;

    public void EnableLaser(){
        active = true;
        transform.GetChild(1).gameObject.SetActive(true);
    }

    public void DisableLaser(){
        active = false;
        transform.GetChild(1).gameObject.SetActive(false);
    }

    public Vector3 GetLaserCollisionPosition(){
        return raycast.origin + raycast.direction * distance;
    }

    void Start(){
        CreateHolder();
        CreatePointer();
        transform.GetChild(1).gameObject.SetActive(false);
    }

    private void CreateHolder(){
        holder = new GameObject();
        holder.transform.parent = this.transform;
        holder.transform.localPosition = Vector3.zero;
    }

    private void CreatePointer(){
        pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointer.transform.parent = holder.transform;
        pointer.transform.localScale = new Vector3(thickness , thickness , 100f);
        pointer.transform.localPosition = new Vector3(0f , 0f , 50f);
        AddRigidbodyToPointer();
        AddMaterialToPointer();
    }

    private void AddRigidbodyToPointer(){
        BoxCollider collider = pointer.GetComponent<BoxCollider>();
        if (addRigidBody){
            if (collider){
                collider.isTrigger = true;
            }
            Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
            rigidBody.isKinematic = true;
        }else{
            if (collider){
                Object.Destroy(collider);
            }
        }
    }

    private void AddMaterialToPointer(){
        pointer.GetComponent<MeshRenderer>().material = materials [ 0 ];
    }

    void Update(){
        if (active){
            SteamVR_TrackedController controller = GetComponent<SteamVR_TrackedController>();
            raycast = new Ray(transform.position , transform.forward);
            RaycastHit hit;
            if (CheckIfRaycastDidHit(out hit)){
                if (hit.collider.tag == "ground"){
                    raycastDidHit = true;
                    pointer.GetComponent<MeshRenderer>().material = materials [ 1 ];
                }else{
                    pointer.GetComponent<MeshRenderer>().material = materials [ 0 ];
                    DisableTPArea();
                    EnableNotTPArea();
                    raycastDidHit = false;
                }
                distance = hit.distance;
                if (previousContact != hit.transform){
                    UpdatePreviousContact(hit , controller);
                }
            }else{
                raycastDidHit = false;
                previousContact = null;
                pointer.GetComponent<MeshRenderer>().material = materials [ 0 ];
                DisableTPArea();
                DisableNotTPArea();
            }if (previousContact && previousContact != hit.transform){
                DeletePreviousContact(hit , controller);
            }
            pointer.transform.localScale = new Vector3(thickness , thickness , distance);
            pointer.transform.localPosition = new Vector3(0f , 0f , distance / 2f);
            if(raycastDidHit) {
                EnableTPArea();
                DisableNotTPArea();
                UpdateTPAreaPosition(GetLaserCollisionPosition());
            }else{ 
                UpdateNotTPAreaPosition(GetLaserCollisionPosition());
            }
        }
    }

    private bool CheckIfRaycastDidHit(out RaycastHit hit){
        return Physics.Raycast(raycast , out hit);
    }

    private void EnableTPArea(){
        tpArea.SetActive(true);
    }

    public void DisableTPArea(){
        tpArea.SetActive(false);
    }
 
    private void UpdateTPAreaPosition(Vector3 position){
        tpArea.transform.position = position;
        Vector3 lookPosition = new Vector3(pointer.transform.position.x , position.y , pointer.transform.position.z);
        tpArea.transform.LookAt(lookPosition);
    }

    public void EnableNotTPArea(){
        notTpArea.SetActive(true);
    }

    public void DisableNotTPArea(){
        notTpArea.SetActive(false);
    }

    private void UpdateNotTPAreaPosition(Vector3 position)
    {
        notTpArea.transform.position = position;
        notTpArea.transform.LookAt(notTpArea.transform.position + GameObject.Find("Camera (eye)").transform.rotation * Vector3.forward , GameObject.Find("Camera (eye)").transform.rotation * Vector3.up);
    }

    private void UpdatePreviousContact(RaycastHit hit , SteamVR_TrackedController controller){
        PointerEventArgs argsIn = new PointerEventArgs();
        if (controller != null){
            argsIn.controllerIndex = controller.controllerIndex;
        }
        argsIn.distance = hit.distance;
        argsIn.flags = 0;
        argsIn.target = hit.transform;
        OnPointerIn(argsIn);
        previousContact = hit.transform;
    }

    private void DeletePreviousContact(RaycastHit hit , SteamVR_TrackedController controller){
        PointerEventArgs args = new PointerEventArgs();
        if (controller != null){
            args.controllerIndex = controller.controllerIndex;
        }
        args.distance = 0f;
        args.flags = 0;
        args.target = previousContact;
        OnPointerOut(args);
        previousContact = null;
    }

    public virtual void OnPointerIn(PointerEventArgs e){
        if (PointerIn != null)
            PointerIn(this , e);
    }

    public virtual void OnPointerOut(PointerEventArgs e){
        if (PointerOut != null)
            PointerOut(this , e);
    }
}

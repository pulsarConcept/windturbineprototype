﻿using UnityEngine;

public class HTC_Vive_Left_Controller_Manager : MonoBehaviour {
    enum States{
        unplugged,
        idle,
        triggerPressed,
        padPressed
    }
    States controllerState = new States();

    void Awake(){
        controllerState = States.unplugged;
    }

    void OnDisable(){
        HTC_Vive_Controllers_Manager.instance.leftController = 0;
        controllerState = States.unplugged;
    }

    void Update(){
        if (HTC_Vive_Controllers_Manager.instance.leftController != 0 && controllerState != States.unplugged){
            ReleasePad();
            PressPad();
            ReleaseTrigger();
            PressTrigger();
        }else if (controllerState == States.unplugged && HTC_Vive_Controllers_Manager.instance.leftController != 0){
            SetControllerIndex();
        }
    }

    void SetControllerIndex(){
        if (checked((int) HTC_Vive_Controllers_Manager.instance.leftController) == 1){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device1;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.leftController) == 2){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device2;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.leftController) == 3){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device3;
        }else if (checked((int) HTC_Vive_Controllers_Manager.instance.leftController) == 4){
            GetComponent<SteamVR_TrackedObject>().index = SteamVR_TrackedObject.EIndex.Device4;
        }
        controllerState = States.idle;
    }

    void PressPad(){
        if (HTC_Vive_Controllers_Manager.instance.TouchPadIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.leftController) && controllerState != States.padPressed && controllerState != States.triggerPressed){
            controllerState = States.padPressed;
        }
    }

    void ReleasePad(){
        if (!HTC_Vive_Controllers_Manager.instance.TouchPadIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.leftController) && controllerState == States.padPressed){
            controllerState = States.idle;
        }
    }

    void PressTrigger(){
        if (HTC_Vive_Controllers_Manager.instance.TriggerIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.leftController) && controllerState != States.triggerPressed && controllerState != States.padPressed){
            controllerState = States.triggerPressed;
        }
    }

    void ReleaseTrigger(){
        if (!HTC_Vive_Controllers_Manager.instance.TriggerIsPressed(SteamVR.instance , HTC_Vive_Controllers_Manager.instance.leftController) && controllerState == States.triggerPressed){
            controllerState = States.idle;
        }
    }
}

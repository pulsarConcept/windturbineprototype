﻿using UnityEngine;
using Valve.VR;

public class HTC_Vive_Controllers_Manager : MonoBehaviour {
    public static HTC_Vive_Controllers_Manager instance;

    public uint leftController { get; set; }
    public uint rightController { get; set; }

    void Awake(){
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    void OnEnable(){
        SteamVR_Utils.Event.Listen("device_connected" , OnDeviceConnected);
    }

    void OnDeviceConnected(params object [ ] args){
        var index = (int) args [ 0 ];
        SteamVR VRInstance = SteamVR.instance;
        if (VRInstance.hmd.GetTrackedDeviceClass((uint) index) == ETrackedDeviceClass.Controller){
            if (leftController == 0){
                leftController = (uint) index;
                Debug.Log("Left Controller connected, index: " + leftController);
            }
            else if (rightController == 0){
                rightController = (uint) index;
                Debug.Log("Right Controller connected, index: " + rightController);
            }
        }
    }

    public bool TouchPadIsPressed(SteamVR VRInstance , uint controller){
        VRControllerState_t state = new VRControllerState_t();
        VRInstance.hmd.GetControllerState(controller , ref state);
        return ( state.ulButtonPressed & SteamVR_Controller.ButtonMask.Touchpad ) != 0;
    }

    public bool TriggerIsPressed(SteamVR VRInstance , uint controller){
        VRControllerState_t state = new VRControllerState_t();
        VRInstance.hmd.GetControllerState(controller , ref state);
        return ( state.ulButtonPressed & SteamVR_Controller.ButtonMask.Trigger ) != 0;
    }
}

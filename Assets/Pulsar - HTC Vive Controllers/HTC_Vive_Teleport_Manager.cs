﻿using UnityEngine;

public class HTC_Vive_Teleport_Manager : MonoBehaviour {

    private int TOTAL_SPAWN_POINTS_NUMBER;
    private int location = 0;
    Transform reference;

    void Awake(){
        TOTAL_SPAWN_POINTS_NUMBER = GameObject.Find("SpawnPoints").transform.childCount;
    }

    void Start(){
        Transform eyeCamera = GameObject.FindObjectOfType<SteamVR_Camera>().GetComponent<Transform>();
        reference = eyeCamera.parent.parent;
    }

    public void TeleportToSpawnPoints(){
        if (location < TOTAL_SPAWN_POINTS_NUMBER){
            location++;
            reference.position = GameObject.Find(location + "-SpawnPoint").transform.position;
        }
        if (location >= TOTAL_SPAWN_POINTS_NUMBER){
            location = 0;
        }
        Blink(0.6f);
    }

    public void TeleportToRayCastHit(){
        if (GameObject.Find("Controller (right)").GetComponent<HTC_Vive_LasterPointer>().raycastDidHit){
            Vector3 newPos = GameObject.Find("Controller (right)").GetComponent<HTC_Vive_LasterPointer>().GetLaserCollisionPosition() - new Vector3(reference.GetChild(0).localPosition.x , 0f , reference.GetChild(0).localPosition.z);
            reference.position = newPos;
            Blink(0.3f);
            GameObject.Find("Controller (right)").GetComponent<HTC_Vive_LasterPointer>().DisableTPArea();
        }else{
            GameObject.Find("Controller (right)").GetComponent<HTC_Vive_LasterPointer>().DisableNotTPArea();
        }
     
    }

    protected virtual void Blink(float time){
        SteamVR_Fade.Start(Color.black , 0);
        SteamVR_Fade.Start(Color.clear , time);
    }
}

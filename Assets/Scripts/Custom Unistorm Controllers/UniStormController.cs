﻿using UnityEngine;
using System.Collections;

public class UniStormController : MonoBehaviour {
    UniStormWeatherSystem_C unistormSystemController;
    void Awake () {
        unistormSystemController = GameObject.Find("UniStormSystemEditor").GetComponent<UniStormWeatherSystem_C>();
        unistormSystemController.temperatureType = 2; //Grados Celsius
        StopDayCycle();
	}
    void OnGUI()
    {
        string snowOrRain;
        if (unistormSystemController.temperature > 0)
            snowOrRain = "Lluvia";
        else
            snowOrRain = "Nieve";
        if (GUI.Button(new Rect(10, 10, 50, 30), "8:00"))
            SetTime(8f);
        if (GUI.Button(new Rect(60, 10, 50, 30), "12:00"))
            SetTime(12f);
        if (GUI.Button(new Rect(110, 10, 50, 30), "17:00"))
            SetTime(17f);
        if (GUI.Button(new Rect(10, 40, 50, 30), "21:00"))
            SetTime(21f);
        if (GUI.Button(new Rect(60, 40, 50, 30), "0:00"))
            SetTime(0f);
        if (GUI.Button(new Rect(110, 40, 50, 30), "03:00"))
            SetTime(3f);
        if (GUI.Button(new Rect(10, 70, 100, 30), "Ciclo Día"))
            StartDayCycle();
        if (GUI.Button(new Rect(10, 100, 50, 30), "Niebla"))
            SetWeatherType(1);
        if (GUI.Button(new Rect(60, 100, 50, 30), snowOrRain + "S"))
            SetWeatherType(2);
        if (GUI.Button(new Rect(110, 100, 50, 30), snowOrRain + "F"))
            SetWeatherType(3);
        if (GUI.Button(new Rect(10, 130, 50, 30), "Nubes"))
            SetWeatherType(4);
        if (GUI.Button(new Rect(60, 130, 50, 30), "Sol"))
            SetWeatherType(8);
        if (GUI.Button(new Rect(10, 160, 100, 30), "Cambiar Temp"))
            ChangeTemperature();
        GUI.Label(new Rect(110f, 160f, 100, 50), "Temperatura: " + unistormSystemController.temperature + "ºC");
    }

    void SetTime(float hour)
    {
        StopDayCycle();
        unistormSystemController.startTime = hour * 0.99f / 23.59f;
    }

    void StartDayCycle()
    {
        unistormSystemController.timeStopped = false;
    }

    void StopDayCycle()
    {
        unistormSystemController.timeStopped = true;
    }

    void SetWeatherType(int weatherType)
    {
        unistormSystemController.weatherForecaster = weatherType;
    }
    void ChangeTemperature()
    {
        unistormSystemController.temperature *= -1;
    }
}

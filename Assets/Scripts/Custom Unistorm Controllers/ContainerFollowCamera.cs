﻿using UnityEngine;
using System.Collections;

public class ContainerFollowCamera : MonoBehaviour {
    public Transform Camera;
    void LateUpdate () {
        transform.position = Camera.position;
	}
}

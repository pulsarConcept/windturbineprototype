﻿using UnityEngine;
using System.Collections;

public class RainParticleFixer : MonoBehaviour {
    public Transform Camera;
	void FixedUpdate () {
        transform.rotation = Quaternion.identity;
        transform.Rotate(Vector3.right , -90);
        transform.position = new Vector3(Camera.position.x , Camera.transform.position. y + 30 , Camera.transform.position.z);
    }
}
